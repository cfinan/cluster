Welcome to hpc-tools
====================

hpc-tools a Python package with some additional bash scripts/configuration files designed to simplify submitting array jobs on a cluster. Whilst it is written with Grid Engine in mind it can probably be re-purposed for other systems (such as LSF) - although this has not been tested in any way. Also, this has only been tested on the University College London (UCL) systems, Myriad and the CS cluster.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started
   workflow
   example_job_array

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing
   package_management

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
