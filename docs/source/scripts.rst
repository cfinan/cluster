======================
Command-line endpoints
======================

Below is a list of all the command line endpoints installed with hpc-tools. Many of these are mentioned in their respective context throughout the documentation but they are listed here all in a single place.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   scripts/pyjasub
   scripts/make_jaf
