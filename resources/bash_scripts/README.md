## Overview of `bash_scripts`
Whilst these do not form part of the python package they are functionally related to the python package and so are place in the repository. There are tow "skeleton" template scripts in here that can be used as models for actual scripts that do something:

### Skeleton scripts
* **`skeleton_job_array_task.sh`** - A skeleton task script for running a single task with an infile and an outfile and a step size of 1. This illustrates many of the attributes outlined [here]()
* **`
