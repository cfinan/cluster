#!/bin/bash
# should be run after the array job has been submitted

# logjob echo "Your job-array 3185216.1-5:1 ("download_file_test_array.sh") has been submitted"

if [[ $FLAGS_cluster -eq 0 ]]; then
   msg "jobid: $JOBID"
   if [ -z $JOBID ]; then
       echo "[error] unable to capture the job ID, this is an error that is difficult to handle, profiling will not wwork, I will exit now"
       exit
   fi

   njobs=$(wc -l "$INIT_RUN_FILE" | awk '{print $1}')
   nrun=$(wc -l "$FINAL_RUN_FILE" | awk '{print $1}')
   nfailed=$((njobs - nrun))

   echo -e "$ALERT\t$DATE_PRETTY\t$RUNTIME\t$PROCESS\t$RUN_NO\t$RUN_NAME\t$WORKING_DIR\t$FLAGS_jafile\t$JAFILE_MD5\t$SCRIPT_LOCATION\t$SCRIPT_MD5\t$RUN_LOG\t$njobs\t$nrun\t\$nfailed\t$JOBID" >> "${EXP_FILES[RUN_LOG]}"

   # if we are profiling then launch it
   if [[ $FLAGS_profile -gt 0 ]]; then
       trap - SIGINT ERR
       profile.sh $JOBID
   fi
else
    JOBID="local"
    echo -e "$ALERT\t$DATE_PRETTY\t$RUNTIME\t$PROCESS\t$RUN_NO\t$RUN_NAME\t$WORKING_DIR\t$FLAGS_jafile\t$JAFILE_MD5\t$SCRIPT_LOCATION\t$SCRIPT_MD5\t$RUN_LOG\t$njobs\t$nrun\t\$nfailed\t$JOBID" >> "${EXP_FILES[RUN_LOG]}"
fi
