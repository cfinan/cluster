#!/bin/bash
# This should be called after initialisation and file testing but before 
# running the job, it will generate several variables that should be 
# used for running the job

# first perform some checks on the FINAL_RUN_FILE to make sure we are 
# good to go
if [ ! -r "$FINAL_RUN_FILE" ]; then
    error $LINENO "final run found not readable: $FINAL_RUN_FILE"
fi

# Now we test to see how many lines (jobs) we have and make sure the 
# columns are consistant

# A single line bug with cct
rows=$(cat "$FINAL_RUN_FILE" | wc -l)
if [[ $rows -gt 1 ]]; then
    COLUMNS=($(cct -d$'\t' "$FINAL_RUN_FILE" | tr ',' ' '))
    msg "final run file, mincols=${COLUMNS[0]}, maxcols=${COLUMNS[1]}, nrows=${COLUMNS[2]}"

    if [[ ${COLUMNS[0]} -ne ${COLUMNS[1]} ]]; then
        error $LINENO "mincols != maxcols in final run file: $FINAL_RUN_FILE"
    fi
fi

# Also make sure we have some rows
if [[ $rows -eq 0 ]]; then
    error $LINENO "0 rows in final run file: $FINAL_RUN_FILE"
fi

# Now we can define the job array start and end
ARRAY_START=1
ARRAY_END=$rows

# if we get here now we can check if the user is profiling, if so then we will
# only extract a random subset of files from the final run file
if [[ $FLAGS_profile -gt 0 ]]; then
    if [[ $FLAGS_profile -gt 50 ]]; then
        echo "Are you sure you want to profile $FLAGS_profile jobs? [CTRL-C to exit]"
        read ok
    fi

    # make a profile directory
    mkdir "$RUN_FILE_PROFILE"

    profile_end=$(echo "$FLAGS_profile * $FLAGS_step" | bc -l)
    if [[ $profile_end -gt $ARRAY_END ]]; then
        msg "profile more jobs (array slots) than you have, setting to $ARRAY_END" "warning"
        profile_end=$ARRAY_END
    fi

    msg "will profile $FLAGS_profile random jobs: in steps of $FLAGS_step"

    # profile file that will be used for the run
    PROFILE_RUN_FILE="$RUN_FILE_PROFILE"/"$RUN_NAME"_profile
    #sort -R "$FINAL_RUN_FILE" | head -n$profile_end > "$PROFILE_RUN_FILE"
    head -qn$profile_end "$FINAL_RUN_FILE" > "$PROFILE_RUN_FILE"

    FINAL_RUN_FILE="$PROFILE_RUN_FILE"
    ARRAY_END=$profile_end
fi

msg "array start: $ARRAY_START, array end: $ARRAY_END"
msg "flags: $FLAGS_auto cluster: $FLAGS_cluster"
# If the user wants the script to run the job
if [[ $FLAGS_auto -eq 0 ]] && [[ $FLAGS_cluster -eq 0 ]]; then
    msg "doing cluster run"
    # We need the actual command to run to be set in an array
    if [[ ${#COMMAND[@]} -eq 0 ]]; then
        error $LINENO 'using --auto but there is no command to run, remember to set the $COMMAND[@]} array'
    fi
    msg "got here 1"

    ## The base QSUB command
    QSUB_COMMAND=("logjob" "qsub" "-t" "${ARRAY_START}"-"${ARRAY_END}":"$FLAGS_step" "-l" "h_rt=""$FLAGS_time" "-S" "$FLAGS_shell" "-o" "$OUTS" "-e" "$ERRORS")

    ## I am not sure if qsub will break if -tc < 0 so I will
    ## specificly test for it and alter the QSUB command
    ## TODO: set qsub -tc -1 to see what it does
    if [[ $FLAGS_batch -gt 0 ]]; then
	QSUB_COMMAND+=("-tc" "$FLAGS_batch")
    fi

    ## Add the memory if required
    if [[ $FLAGS_mem != '0' ]]; then
	QSUB_COMMAND+=("-l" "h_vmem=""$FLAGS_mem" "-l" "tmem=""$FLAGS_mem")
    fi

    ## If a subset of nodes have been specified, then add them here
    if [[ $FLAGS_nodes != '' ]]; then
	QSUB_COMMAND+=("-l" "hostname=""$FLAGS_nodes")
    fi
    
    msg "got here 2"
    # If the scratch command is set then we build the command flag
    if [[ ! -z $FLAGS_scratch ]]; then
        msg "using --scratch $FLAGS_scratch"
         QSUB_COMMAND+=(-l tscratch="${FLAGS_scratch}")
    fi
    msg "got here 3"
    QSUB_COMMAND+=("${COMMAND[@]}")
    msg "got here 4"
    # Put the command through echo otherwise it screws up msg
    msg "Your sub command: $(echo ${QSUB_COMMAND[@]})"
    echo "ready to run $ARRAY_END jobs, are you sure you want to continue? [ENTER or CTRL-C to exit]"
    read ok
    "${QSUB_COMMAND[@]}"
fi

if [[ $FLAGS_cluster -eq 1 ]]; then
    ALERT="$ALERT""L"
    # Put the command through echo otherwise it screws up msg
    msg "running locally your command is: $(echo ${COMMAND[@]})"
    echo "ready to run local job continue using ($FLAGS_local_job_id)? [ENTER or CTRL-C to exit]"
    read ok
    SGE_TASK_ID=$FLAGS_local_job_id
    export SGE_TASK_ID
    /usr/bin/time -v "${COMMAND[@]}"
fi
