#!/bin/bash
###############################################################################
# A generic script to run/re-run job arrays                                   #
###############################################################################

SCRIPT_LOCATION="$( readlink -f "${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]}" )"
PROG_DIR="$( dirname "$SCRIPT_LOCATION")"
PROG_NAME="$(basename "$SCRIPT_LOCATION")"
VERSION="0.1"

################################################################################
############################ FUNCTIONS ARE HERE ################################
################################################################################

# The usage instructions, these will be output if the user gets anything wrong
USAGE=`cat << EOF
${PROG_NAME} v$VERSION
usage: ${PROG_NAME} options [infiles]

[infiles]  One or more input files that will be checked (optional). If these are not defined then either '--jafile' should be given or '--custom' get_files function should be passed.

The idea behind this script is to take a load of input files and see if the potental output files pass a series of tests. If they don't then they form the input for a cluster job array, if they do then they are not run.

problems: c.finan@ucl.ac.uk

EOF
`

# Don't set trap errors before using this otherwise it doesn't work properly
# this sources in shflags. This is a third party code for streamlining bash 
# arguments and seems to work well. This should be in your path.
. shflags || echo $USAGE

# Default argument values here
DEFINE_string 'jafile' '' 'a file with job array parameters, input file is first column, output file is second column' 't'
DEFINE_string 'outpath' '' 'if provided an output file with the basename as the input will be tested in this path' 'p'
DEFINE_string 'runfile' "${PWD}/ja_run.txt" 'the job array run file for the output file files that have failed' 'r'
DEFINE_integer 'osize_ge' -1 'test if the output file size is >= --osize_ge kb (< 0 = off)'
DEFINE_integer 'isize_ge' -1 'test if the input file size is >= --isize_ge kb (< 0 = off)'


DEFINE_boolean 'outexist' false 'test if the output files exist' 'o'
DEFINE_boolean 'inexist' true 'test if the input files exist' 'i'
DEFINE_boolean 'verbose' false 'do you want to print some info' 'v'
DEFINE_boolean 'xverbose' false 'do you want to print loads of info' 'x'
