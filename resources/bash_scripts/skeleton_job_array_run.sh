#!/bin/bash
# An example of running
# ./extract_pvalue_job_array.sh
trap "exit 1" ERR

##########################################################
# UPDATE THE SCRIPT WITH THE LOCATION OF THE TASK SCRIPT #
##########################################################
# This is my generic (atomic) process script that actually does the work
SCRIPT="<YOUR TASK SCRIPT WILL GO HERE>.sh"

# Make sure the script exists
if [[ ! -e $(which "$SCRIPT") ]]; then
    echo "[fatal] can't find the task script" 1>&2
    exit 1
fi

#==============================================================================

# For command line argument parsing
. shflags || echo $USAGE

# Define your command line arguments here
# DEFINE_string 'long_opt' 'default' 'description' 'short_opt'
# DEFINE_integer 'long_opt' 'default' 'description' 'short_opt'
# DEFINE_boolean 'long_opt' 'default' 'description' 'short_opt'
# DEFINE_float 'long_opt' 'default' 'description' 'short_opt'

# This should be sourced in to check the job and initialise the run
# always AFTER you have defined your options
. initialise_job.sh

# After initialisation there are a couple of important variables
# FORCE_RUN - 0 means that the user has supplied a job array file that they
#             want to just run with no checks so you should check it in your
#             script. this is a one time overide of the logged job array file.
#             They might want to do this if they have had to do some manual
#             checking elsewhere, maybe even running another job to check 
#             the outputs of a previous job. 1 means the logged job array is
#             being used
# INIT_RUN_FILE - This is the file that you should perfom any pre-submission 
#                 checks on, it is basically a copy of the logged job array
#                 that is stored in the run log directory
# FINAL_RUN_FILE - This is the file you should use to output any run checking 
#                  into
# ERRORS - This should be used as the argument to qsub -o
# OUTS - This should be used as the argument to qsub -e
# VERBOSE - this will either be a '-v' or "" depending on FLAGS_verbose
#           this can be used in ancillary scripts to respect verbosity
# TEMP - The location of the tmp directory for the processing task script

# Now we respect the force run variable, we don't have to through...
if [ $FORCE_RUN -eq 1 ]; then
    # In this instance we are just checking that the input file is present
    # Note that I have removed --inexist as jab_array_tests can't handle
    # multiple delimited job array files
    "job_array_tests.sh" $VERBOSE --outexist --runfile "$FINAL_RUN_FILE" --jafile "$INIT_RUN_FILE"
else
    # with no checks the initial and the final file are the same
    cp "$INIT_RUN_FILE" "$FINAL_RUN_FILE"
    if [[ $? -ne 0 ]]; then
        error $LINENO "problem copying run file"
    fi
fi

# COMMAND is used by pre_run.sh
# Here we are adding the task script, the final run job array, the temp
# directory location and the step size for the job array. The SGE_TASK_ID
# is supplied as a default arg when the task is run
COMMAND=("$SCRIPT" "$FINAL_RUN_FILE" "$TEMP" "$FLAGS_step")
. pre_run.sh
. post_run.sh
msg "*** END ***"
