#!/bin/bash
# Source in the task setup, this will deal with the command line input
# and cleanup functions, there are several variables that it will give
# access to:
# JA="$1"
# TMP_LOC="$2"
# STEP=$3
# IDX=${4:-$SGE_TASK_ID}

# WORKING_DIR : A location 

# Store all the paths to the intermediate files, these will all be deleted when
# exitting cleanly or with an error. The "" is to stop unbound errors
# TEMPFILES=("")

# Files that should be moved should go into these two arrays. This will then be
# moved to their final location on exit.
# MOVE_FROM=()
# MOVE_TO=()

# PROCESS_LINE : An array extracted from the job array file, this provides
# input for the current STEP of the starting from the current TASK

# ROW_IDX : Element 0 from the process line, this is a requirement that all
# job arrays have this


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Decompose the line array, this is really for code clarity
    INFILE="${PROCESS_LINE[1]}"
    OUTFILE="${PROCESS_LINE[2]}"

    info_msg "infile=""${INFILE}"
    info_msg "outfile=""${OUTFILE}"

    OUTFILE_BASE="$(basename "$OUTFILE")"
    TEMP_OUTFILE="$WORKING_DIR"/"$OUTFILE_BASE"

    # Add the outfiles to our moving arrays
    MOVE_FROM+=("$TEMP_OUTFILE")
    MOVE_TO+=("$OUTFILE")

    # Also add to temp files just in case we exit with an error
    TEMPFILES+=("$TEMP_OUTFILE")

    APPLICATION="md5sum"
    info_msg "running application: ${APPLICATION}"

    "$APPLICATION" "$INFILE" > "$TEMP_OUTFILE"
    info_msg "finished running application: ${APPLICATION}"
}

. task_setup.sh
