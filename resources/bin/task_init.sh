############################################################
# A template task script for running an a single step task  #
#############################################################
# Put in a random sleep so not all jobs start at the same time, this will sleep
# between 0-30s
# sleep $(( $RANDOM % 30 ))

# ARGUMENT 1: job array file name
# ARGUMENT 2: the tmp location, when running a live job this will be /Scratch
#             or ~/scratch0 depending on the cluster
# ARGUMENT 3: The step size for the job array, this is supplied so the task
#             script can use it in loops in combination with SGE_TASK_ID
# ARGUMENT 4: the task index value to run. If debugging this can be provided
#             at the command line. When running a live tasl then it will be the
#             SGE_TASK_ID environment variable of the currently running task
JA="$1"
TMPDIR="$2"
STEP=$3
IDX=${4:-$SGE_TASK_ID}

# Represent TMPDIR in full before bh_init.sh (it should not matter but I am
# just paranoid)
TMPDIR="$(realpath "$TMPDIR")"

# A template for the temp working directory that will be created to hold the
# output
TEMPLATE="run_${USER}_${IDX}_XXXXXXXXXX"

# Switch on verbosity
VERBOSE=0

# Initialise everything, from here on in we have set -Euoe pipefail and
# ERROR/EXIT traps initialised
. bh_init.sh

echo_prog_name "$0"

# Make sure the temp directory is represented as a full path
JA="$(readlink -f "$JA")"

# Make sure the job array is accessable
check_file_readable "$JA" "job array file not readable"
check_dir_writable "$TMPDIR" "temp directory not writable"
check_positive "$STEP" "step value must be > 0"
check_positive $(( IDX + 1 )) "IDX value must be >= 0"

info_msg "job array file=$JA"
info_msg "temp location=""$TMPDIR"
info_msg "step=""$STEP"
info_msg "job idx line (SGE_TASK_ID)="$IDX
info_msg "working directory template="$TEMPLATE
info_msg "working directory=""$WORKING_DIR"

# This is the stop condition at which we will stop processing steps
STOP=$(( IDX + STEP ))

# While we are below our step count
while [[ $IDX -lt $STOP ]]; do
    # Initialise the line number that we want to grab from the job array
    # file and the line we want to stop at (we need this or sed will keep
    # looping through the file)
	  PULL_LINE=$((IDX + 1))
	  QUIT_LINE=$((PULL_LINE + 1))
	  info_msg "pull line (accounting for header)="$PULL_LINE
	  info_msg "quit line="$QUIT_LINE

    # Grab the line from the job array file that we want
	  PROCESS_LINE=($(sed -n "${PULL_LINE}p;${QUIT_LINE}q" "$JA"))
	  info_msg "process line length="${#PROCESS_LINE[@]}

    # This will happen if we are off the end of the job array so we break the
    # loop
    if [[ ${#PROCESS_LINE[@]} -eq 0 ]]; then
		    break
	  fi

    # The first column in a job array file must be the sequential row counter
    ROW_IDX="${PROCESS_LINE[0]}"

    # Make sure that the the IDX == the ROWIDX it should do in all cases
    if [[ $IDX -ne $ROW_IDX ]]; then
        error_msg "idx (SGE_TASK_ID) [${IDX}] != rowidx [${ROW_IDX}]"
        error_exit "$LINENO"
    fi

    info_msg "idx (SGE_TASK_ID+STEP) [${IDX}] == rowidx [${ROW_IDX}]"

    # Now run the task
    run_command
	  IDX=$((IDX + 1))
done
