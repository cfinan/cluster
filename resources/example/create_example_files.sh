#!/bin/bash
###############################################################################
# Create some example input files to run as a small job array on the cluster  #
###############################################################################
root_dir="${HOME}"/"hpc_tools_examples"

if [[ -e "$root_dir" ]]; then
    echo "[error] $root_dir already exists" 1>&2
    exit 1
fi

mkdir "$root_dir"

directories=(alfa bravo charlie delta echo foxtrot golf hotel india juliett)
words=(one two three four five six seven eight nine ten)
numbers=(1 2 3 4 5 6 7 8 9 10)

file_prefix="i_am_a_big_long_file_name_with"
file_suffix="and_all_this_as_well"
input_dir="$root_dir"/"test_input"
mkdir "$input_dir"

for i in "${!directories[@]}"; do
    # Create a subdirectory from the phoneics
    dirname="$input_dir"/"${directories[$i]}"
    mkdir "$dirname"

    # Create a filename from the prefix words suffix and numbers
    filename="$dirname"/"${file_prefix}"_"${numbers[$i]}"_"$file_suffix"_"${words[$i]}.txt"

    # Add some data to the file
    echo "This is the file: $filename" > "$filename"
done

# We will also copy
cp -r ../example "$root_dir"

echo "*** END ***"
