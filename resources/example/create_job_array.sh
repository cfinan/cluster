#!/bin/bash
# Create a job array file and output directory structure
make_general_job_array_file -v \
	                    -i ~/hpc_tools_examples/test_input \
			    -g '*/*.txt' \
			    -o ~/hpc_tools_examples/test_output \
			    --regexp 'i_am_a_big_long_file_name_with_(?P<B>\d+)_and_all_this_as_well_(?P<A>\w+)\.txt' \
			    --levels 1 \
			    --strip_ext \
			    --suffix ".out" \
			    --outfile "./example_job_array.job"
