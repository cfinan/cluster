# Mapping Files
Some example mapping files, mapping cluster arguments to python hpc-tools arguments. The path to your mapping file should be in your `~/.bashrc`, i.e. something like this:

```
export QSUB_PYTHON_MAPPING="${HOME}/configs/my_mappings.cnf"
```
